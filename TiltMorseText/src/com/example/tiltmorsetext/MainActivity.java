//Created by Kelly Kwan for Informatics 133, Android Sensor App: Option #2, December 2014
package com.example.tiltmorsetext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements OnClickListener{
	private TextView mTextViewX1;
	private TextView mTextViewY1;
	private TextView mTextViewZ1;
	private TextView mTextViewX2;
	private TextView mTextViewY2;
	private TextView mTextViewZ2;
	private SensorManager mSensorManager2;
	private SensorManager mSensorManager3;
	private SensorEventListener mEventListenerGyro;
	private SensorEventListener mEventListenerAccel;
	protected float x1;
	protected float y1;
	protected float z1;
	protected float x2;
	protected float y2;
	protected float z2;
	private TextView input;
	private TextView notes;
	private TextView trans;
	private EditText translation;
	private String toAppend = "";
	private boolean morseChanged = false;
	private String note = "Tilt the phone!";
	private String myMorse = "";
	private String myTranslation = "";
	private Button help_button;
	private static String[] TRANSLATION = {"a","b","c","d","e","f","g","h","i","j",
									       "k","l","m","n","o","p","q","r","s","t",
									       "u","v","w","x","y","z","0","1","2","3",
									       "4","5","6","7","8","9",",",".","?","/",
									       ":","-","'"
									      };
	private static int MORSESIZE = 43; //must manually set to length of translation/morse array
	private static String[] MORSE = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---",
								     "-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-",
								     "..-","...-",".--","-..-","-.--","--..","-----",".----","..---","...--",
								     "....-",".....","-....","--...","---..","----.","--..--",".-.-.-","..--..","-..-.",
								     "---...","-....-",".----."
									};
	
	private void updateUI(){
		runOnUiThread(new Runnable(){
			@Override
			public void run(){
				mTextViewX1.setText(String.format("X-axis: %.5f", x1)); 
				mTextViewY1.setText(String.format("Y-axis: %.5f", y1));
				mTextViewZ1.setText(String.format("Z-axis: %.5f", z1));
				mTextViewX2.setText(String.format("X-axis: %.5f", x2)); 
				mTextViewY2.setText(String.format("Y-axis: %.5f", y2));
				mTextViewZ2.setText(String.format("Z-axis: %.5f", z2));
				input.setText(myMorse); //where the morse shows (input)
				notes.setText(note);
//				trans.setText(myTranslation);
				trans.setText("");
//				translation.setText(myTranslation); //where the user is typing
//				translation.append(myTranslation);
				if (morseChanged == true){ //if the input changed because of morse input
					translation.setText(myTranslation); //update the translated string
					morseChanged = false;
					translation.setSelection(translation.getText().length()); //move cursor to the end of the string

				}
				else if(!translation.getText().toString().equals(myTranslation)) //if the user used they keyboard, check if the string matches the translation
				{
					translation.append(toAppend); //if the user input something through morse after keyboard input, add the morse at the end of the keyboard input
					myTranslation = translation.getText().toString(); //update the translation information
					toAppend = "";
				}
			}
		});
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		help_button = (Button) findViewById(R.id.helpOption);
		help_button.setOnClickListener(this);
		mTextViewX1 = (TextView) findViewById(R.id.GyroX);
		mTextViewY1 = (TextView) findViewById(R.id.GyroY);
		mTextViewZ1 = (TextView) findViewById(R.id.GyroZ);
		mTextViewX2 = (TextView) findViewById(R.id.AccelX);
		mTextViewY2 = (TextView) findViewById(R.id.AccelY);
		mTextViewZ2 = (TextView) findViewById(R.id.AccelZ);
		input = (TextView) findViewById(R.id.textViewMorse); //the morse input by the user
		notes = (TextView) findViewById(R.id.textViewNotes); //notes to guide the user
		trans = (TextView) findViewById(R.id.textViewTrans); //A TextView version of the EditText Translation, helpful for debugging 
		translation = (EditText) findViewById(R.id.editText1); //the input field
		translation.setSelection(translation.getText().length()); //move cursor to end of string
		mSensorManager2 = (SensorManager) getSystemService(SENSOR_SERVICE); //one sensor manager for the gyroscope
		mSensorManager3 = (SensorManager) getSystemService(SENSOR_SERVICE); //one sensor manager for the accelerometer

		mEventListenerGyro = new SensorEventListener(){
			@Override
			public void onSensorChanged (SensorEvent event){
				float[] values = event.values;
				x1 = values[0];
				y1 = values[1];
				z1 = values[2];
				if (y1 > 6 && x2 < -4 && x2 > -9.99){ //tilt right more, "dah"
					if(myMorse.length() > 9)
						note = "Morse code characters are not that long!";
					else{
						myMorse += "-";
						note = "Tilt the phone!";
					}
				}
				else if (y1 > 2 && y1 < 6 && x2 < -2 && x2 > -4){ //tilt right, "dit"
					if(myMorse.length() > 9)
						note = "Morse code characters are not that long!";
					else{
						myMorse += ".";
						note = "Tilt the phone!";
					}
				}
				if (y1 < -5 && myMorse.length() > 0 && x2 > 5 ) //tilt left, morse backspace
					myMorse = myMorse.substring(0, myMorse.length() - 1);
				
				if (z1 > 3 && myTranslation.length() > 0 && x2 > 3){ //rotate left, string backspace
					myTranslation = myTranslation.substring(0, myTranslation.length() - 1);
					morseChanged = true;
				}
				
				if (z1 < -2 && myTranslation.length() > 0 && x2 < -3 && x2 > -6){ //rotate right, character capitalize
					myTranslation = myTranslation.substring(0,myTranslation.length() - 1) + myTranslation.substring(myTranslation.length() - 1).toUpperCase();
					morseChanged = true;
				}
				
				if (x1 > 2 && y2 > 7 && z2 < 0){ //tilt toward self, upward
					myTranslation += " "; //space
					morseChanged = true;
				}

				if (x1 < -2 && y2 > -6 && y2 < -0.5){ //tilt away from self, downward to translate morse to text
					doTranslation();
				}

				updateUI();
			}

			@Override
			public void onAccuracyChanged(Sensor arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
		};
		mEventListenerAccel = new SensorEventListener(){
			@Override
			public void onSensorChanged (SensorEvent event){
				float[] values = event.values;
				x2 = values[0];
				y2 = values[1];
				z2 = values[2];
				updateUI();
			}

			@Override
			public void onAccuracyChanged(Sensor arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
		};
	}
	
	public void doTranslation(){
		int translationIndex = -1;
		for(int i = 0; i < MORSESIZE; i++){
			if (myMorse.equals(MORSE[i])){
				translationIndex = i;
				break;
			}
		}
		
		if (translationIndex == -1)
			if(myMorse == "")
				note = "Please input something to translate!";
			else
				note = "Invalid morse code cannot be translated.";
		else{
			myTranslation += TRANSLATION[translationIndex];
			toAppend = TRANSLATION[translationIndex];
			myMorse = "";
			note ="Tilt the phone!";
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onResume(){
		super.onResume();

		mSensorManager2.registerListener(mEventListenerGyro,
				mSensorManager2.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
				SensorManager.SENSOR_DELAY_UI);
		
		mSensorManager3.registerListener(mEventListenerAccel,
				mSensorManager3.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);
	}
	
	
	public void onStop(){
		mSensorManager2.unregisterListener(mEventListenerGyro);
		mSensorManager3.unregisterListener(mEventListenerAccel);
		super.onStop();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.helpOption:
			Intent k = new Intent(MainActivity.this, HelpActivity.class);
			startActivity(k);
			break;
		default:
			break;
		}		
	}

}
