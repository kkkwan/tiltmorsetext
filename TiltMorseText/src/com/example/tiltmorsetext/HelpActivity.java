package com.example.tiltmorsetext;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class HelpActivity extends Activity implements OnClickListener{
	
	public Button cancel_button;
	public ImageView tutorial_image;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
//        tutorial_image = (ImageView) findViewById(R.id.guide_image);
		cancel_button = (Button) findViewById(R.id.option_cancelbutton);
		cancel_button.setOnClickListener(this);
	}
	
	
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.option_cancelbutton:
			finish();
			break;
		default:
			break;
		}		
	}
}