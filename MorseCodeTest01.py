'''
Created on Dec 4, 2014

@author: Kelly

This only tests the alphabet, numbers, and symbols: ? , . /
'''
import random

#creates the morse dictionary
def createMorseDict():
    morseDict = {}
    morseDict[','] = '--..--'#1
    morseDict['.'] = '.-.-.-'#2
    morseDict['/'] = '-..-.' #3
    
    morseDict['0'] = '-----' #4
    morseDict['1'] = '.----' #5
    morseDict['2'] = '..---' #6
    morseDict['3'] = '...--' #7
    morseDict['4'] = '....-' #8
    morseDict['5'] = '.....' #9
    morseDict['6'] = '-....' #10
    morseDict['7'] = '--...' #11
    morseDict['8'] = '---..' #12
    morseDict['9'] = '----.' #13
    morseDict['?'] = '..--..'#14

    morseDict['a'] = '.-'   #15
    morseDict['b'] = '-...' #16 
    morseDict['c'] = '-.-.' #17 
    morseDict['d'] = '-..'  #18
    morseDict['e'] = '.'    #19
    morseDict['f'] = '..-.' #20
    morseDict['g'] = '--.'  #21
    morseDict['h'] = '....' #22
    morseDict['i'] = '..'   #23
    morseDict['j'] = '.---' #24
    morseDict['k'] = '-.-'  #25
    morseDict['l'] = '.-..' #26
    morseDict['m'] = '--'   #27
    morseDict['n'] = '-.'   #28
    morseDict['o'] = '---'  #29
    morseDict['p'] = '.--.' #30
    morseDict['q'] = '--.-' #31
    morseDict['r'] = '.-.'  #32
    morseDict['s'] = '...'  #33
    morseDict['t'] = '-'    #34
    morseDict['u'] = '..-'  #35
    morseDict['v'] = '...-' #36
    morseDict['w'] = '.--'  #37
    morseDict['x'] = '-..-' #38
    morseDict['y'] = '-.--' #39
    morseDict['z'] = '--..' #40

    return morseDict

def morseToText(dict,prog,correct,incorrect,total):
    rand = random.randrange(0, len(dict))
    i = 0
    for char, morse in sorted(dict.items()):
        testPair = [char, morse]
        i += 1
        if i == rand:
            break
    
    print('Morse:', testPair[1])        
    userIn = input('Text: ')
    if userIn.lower() == 'm2t' or userIn.lower() == 't2m' or userIn.lower() == 'quit' or userIn.lower() == 'exit' or userIn.lower() == 'life':
        if userIn.lower() == 'life':
            incorrect -= 1
            total -= 1
    elif userIn.lower() == testPair[0]:
        print('Correct!\n')
        prog.add(testPair[0])
        correct += 1
        total += 1
    else:
        print('Answer:', testPair[0],'\n')
        prog.discard(testPair[0])
        incorrect += 1
        total += 1

    
    return [userIn,correct,incorrect,total]
            
def textToMorse(dict,prog,correct,incorrect,total):
    rand = random.randrange(0, len(dict))
    i = 0
    for char, morse in sorted(dict.items()):
        testPair = [char, morse]
        i += 1
        if i == rand:
            break
    
    print('Text:', testPair[0])        
    userIn = input('Morse: ')
    if userIn.lower() == 'm2t' or userIn.lower() == 't2m' or userIn.lower() == 'quit' or userIn.lower() == 'exit' or userIn.lower() == 'life':
        if userIn.lower() == 'life': #for those times when you "accidentally" type the wrong answer and want to undo
            incorrect -= 1 #try not to abuse this :P
            total -= 1
    elif userIn.lower() == testPair[1]:
        print('Correct!\n')
        prog.add(testPair[0])
        correct += 1
        total += 1

    else:
        print('Answer:', testPair[1],'\n')
        prog.discard(testPair[0])
        incorrect += 1
        total += 1

    
    return [userIn,correct,incorrect,total]

            
if __name__ == "__main__":
    dict = createMorseDict()
    lesson = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','.',',','?','/'}
    prog = set()
    total = 0
    correct = 0
    incorrect = 0
    userIn = ' '
    test = 'm2t'
        
    print("Welcome to the Morse Code Test.")
    print("Type 'QUIT' or 'EXIT' to stop.")
    print("Type 'M2T' or 'T2M' to switch tests.")

    while not userIn.lower() == 'quit' and not userIn.lower() == 'exit':
        if prog == lesson:
            print('End of test.')
            break
        
        if userIn.lower() == 'm2t':
            print('m2t intiated')
            test = 'm2t'
            
        elif userIn.lower() == 't2m':
            test = 't2m'
            print('t2m intiated')
        
        if test == 'm2t':
            results = morseToText(dict,prog,correct,incorrect,total)
            userIn = results[0]
            correct = results[1]
            incorrect = results[2]
            total = results[3]
        elif test == 't2m':
            results = textToMorse(dict,prog,correct,incorrect,total)
            userIn = results[0]
            correct = results[1]
            incorrect = results[2]
            total = results[3]
        
        print('Progress:', len(prog))
        print(sorted(list(prog)))
        

    print('You were tested on', total, 'questions.')
    print('You got {} ({:.2f}%) correct.'.format(correct,(correct/total)*100))
    print('You got {} incorrect.'.format(incorrect))
            
        
        