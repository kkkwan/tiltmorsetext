Option 2 Android Project for INF 133 at UCI.

See course webpage for more details on the two options.
http://www.ics.uci.edu/~djp3/classes/2014_09_INF133/tasks/task_android.html

" -Build a text-input alternative to a keyboard that uses the phone's orientation
  -This would be a program that allows you to construct a string by turning the phone and hitting at most one button."
  
My implementation constructs a string only using the phone's gyroscope and accelerometer sensors and Morse Code.
A button in the app for a tutorial (made by yours truely :) ) is also included.

I also included a little game I wrote to train myself to read Morse Code so I could demo the project more smoothly. 

This project was created in Eclipse, right as the first stable build of Android Studio was released. 
